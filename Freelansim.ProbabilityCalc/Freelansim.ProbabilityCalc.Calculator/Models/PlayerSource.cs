﻿using System.Collections.Generic;
using Freelansim.ProbabilityCalc.Calculator.Interfaces;

namespace Freelansim.ProbabilityCalc.Calculator.Models
{
    public class PlayerSource
    {
        public List<IStatistics> LastYearTours { get; set; }

        public List<IStatistics> LastTenTours { get; set; }

        public long Id { get; set; }

        public int Points { get; set; }
        
        public string Name { get; set; }

        public string Court { get; set; }

        public double Coefficient { get; set; }
    }
}