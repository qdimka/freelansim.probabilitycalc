﻿using System;
using Freelansim.ProbabilityCalc.Calculator.Interfaces;

namespace Freelansim.ProbabilityCalc.Calculator.Models
{
    public class Statistics : IStatistics
    {
        #region Игрок

        public long Id { get; set; }

        public double Aces { get; set; }

        public double F1W { get; set; }

        public double F2W { get; set; }

        public double BPW { get; set; }

        public double RPW { get; set; }

        public bool IsWinner { get; set; }

        public double Aw => (F1W + F2W) / 2;

        #endregion

        #region Турнир

        public long TourId { get; set; }

        public string Court { get; set; }

        public DateTime TourDate { get; set; }

        #endregion
    }
}
