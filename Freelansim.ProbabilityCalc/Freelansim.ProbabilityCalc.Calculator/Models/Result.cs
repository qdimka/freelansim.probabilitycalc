﻿using System;

namespace Freelansim.ProbabilityCalc.Calculator.Models
{
    public class Result
    {
        public PlayerResult First { get; set; }
        
        public PlayerResult Second { get; set; }

        public Func<double, double> ToView = (value) => Math.Round(value * 100, MidpointRounding.AwayFromZero);

        public override string ToString()
        {
            return $"{First.Name}-{Second.Name};{ToView(First.ProbabilityByLastTen)};{ToView(Second.ProbabilityByLastTen)}; " +
                   $"{ToView(First.ProbabilityByYear)};{ToView(Second.ProbabilityByYear)};" +
                   $"{ToView(First.AverageProbability)};{ToView(Second.AverageProbability)};"+
                   $"{ToView(First.MEByYear)};{ToView(Second.MEByYear)};" +
                   $"{ToView(First.MEByLastTen)};{ToView(Second.MEByLastTen)};" +
                   $"{ToView(First.MEByAverage)};{ToView(Second.MEByAverage)};" +
                   $"{First.Criterion()};{Second.Criterion()};";
        }
    }
    
    public class PlayerResult
    {
        public string Name { get; set; }

        public double Coefficient { get; set; }
        
        public double ProbabilityByYear { get; set; }

        public double ProbabilityByLastTen { get; set; }

        public double AverageProbability => (ProbabilityByLastTen + ProbabilityByYear) / 2;

        public double MEByYear => ProbabilityByYear * Coefficient;

        public double MEByLastTen => ProbabilityByLastTen * Coefficient;

        public double MEByAverage => AverageProbability * Coefficient;

        public double Criterion()
            => ((Coefficient * AverageProbability) - 1) / (Coefficient - 1);

    }
}