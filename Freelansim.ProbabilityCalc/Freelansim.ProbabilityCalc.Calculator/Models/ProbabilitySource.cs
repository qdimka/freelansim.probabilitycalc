﻿namespace Freelansim.ProbabilityCalc.Calculator.Models
{
    public class ProbabilitySource
    {
        public PlayerSource FirstPlayer { get; set; }
        
        public PlayerSource SecondPlayer { get; set; }
    }
}