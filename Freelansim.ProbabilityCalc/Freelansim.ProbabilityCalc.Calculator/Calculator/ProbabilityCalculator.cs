﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Freelansim.ProbabilityCalc.Calculator.Interfaces;
using Freelansim.ProbabilityCalc.Calculator.Models;

namespace Freelansim.ProbabilityCalc.Calculator.Calculator
{
    public class ProbabilityCalculator : IProbabilityCalculator
    {
        public ProbabilityResult Calculate(List<ProbabilitySource> data)
        {
            return new ProbabilityResult()
            {
                Results = data
                    .Select(x => CalculateInternal(x.FirstPlayer, x.SecondPlayer))
                    .ToList()
            };
        }

        public async Task<ProbabilityResult> CalculateAsync(List<ProbabilitySource> data) =>
            await Task.Factory.StartNew(() => Calculate(data));

        #region Average

        private double CalculateAverage(IEnumerable<double> parameters)
            => parameters.Sum() / parameters.Count();

        public AverageParameters CalculateAvgParameters(List<IStatistics> statisticsByPeriod)
        {
            return new AverageParameters()
            {
                AvgAces = CalculateAverage(statisticsByPeriod.Select(x => x.Aces)),
                Avg2w = CalculateAverage(statisticsByPeriod.Select(x => x.F2W)),
                AvgAw = CalculateAverage(statisticsByPeriod.Select(x => x.Aw)),
                AvgBpw = CalculateAverage(statisticsByPeriod.Select(x =>x.BPW)),
                AvgRpw = CalculateAverage(statisticsByPeriod.Select(x =>x.RPW))
            };
        }

        #endregion

        #region Common

        private double CalculateWinStats(List<IStatistics> player, string courtName)
            => player.Count != 0
                ? player.Count(x => x.IsWinner && x.Court == courtName) / player.Count
                : 0;

        private double CalculateAdv(double firstParameter, double secondParameter)
        {
            if (Math.Abs(firstParameter) < 0.001 && Math.Abs(secondParameter) < 0.001)
                return 0;
            
            return firstParameter/(firstParameter + secondParameter);
        }

        private double CalculatePoints(double firstPoint, double secondPoint)
            => CalculateAdv(Math.Pow(firstPoint, Math.E), Math.Pow(secondPoint, Math.E));

        public CommonParameters CalculateCommonParameters(AverageParameters first, AverageParameters second,
            double firstWinStat, double secondWinStat, int pointsFirst, int pointsSecond)
        {
            var serveAdvFirst = first.AvgAw - second.AvgRpw;
            var serveAdvSecond = second.AvgAw - first.AvgRpw;
            
            return new CommonParameters()
            {
                ServeAdvFirstPlayer = serveAdvFirst,
                ServeAdvSecondPlayer = serveAdvSecond,
                ServeAdv = CalculateAdv(serveAdvFirst, serveAdvSecond),

                TwoW = CalculateAdv(first.Avg2w, second.Avg2w),
                BPW = CalculateAdv(first.AvgBpw, second.AvgBpw),
                Aces = CalculateAdv(first.AvgAces, second.AvgAces),

                WinnerFirstPlayer = firstWinStat,
                WinnerSecondPlayer = secondWinStat,

                Win = CalculateAdv(firstWinStat, secondWinStat),
                
                Points = CalculatePoints(pointsFirst, pointsSecond)
            };
        }

        #endregion

        #region Calculate

        private Result CalculateInternal(PlayerSource firstPlayer, PlayerSource secondPlayer)
        {
            #region LastTen

            var commonResultByLastTen = CalculateByPeriod(firstPlayer, secondPlayer, firstPlayer.LastTenTours,
                secondPlayer.LastTenTours); 
            
            #endregion
            
            #region LastYear

            var commonResultByYear = CalculateByPeriod(firstPlayer, secondPlayer, firstPlayer.LastYearTours,
                secondPlayer.LastYearTours);
            
            #endregion

            return new Result
            {
                First = new PlayerResult()
                {
                    Name = firstPlayer.Name,
                    ProbabilityByLastTen = commonResultByLastTen.Calculate(),
                    ProbabilityByYear = commonResultByYear.Calculate(),
                    Coefficient = firstPlayer.Coefficient,
                },

                Second = new PlayerResult()
                {
                    Name = secondPlayer.Name,
                    ProbabilityByLastTen = (1 - commonResultByLastTen.Calculate()),
                    ProbabilityByYear = (1 - commonResultByYear.Calculate()),
                    Coefficient = secondPlayer.Coefficient,
                }
            };
        }

        private CommonParameters CalculateByPeriod(PlayerSource firstPlayer, PlayerSource secondPlayer,
            List<IStatistics> firstByPeriod, List<IStatistics> secondByPeriod)
        {
            var firstAverageResult = CalculateAvgParameters(firstByPeriod);
            var secondAverageResult = CalculateAvgParameters(secondByPeriod);

            var firstWinStat = CalculateWinStats(firstByPeriod, firstPlayer.Court);
            var secondWinStat = CalculateWinStats(secondByPeriod, secondPlayer.Court);

            return CalculateCommonParameters(firstAverageResult, secondAverageResult, firstWinStat,
                secondWinStat, firstPlayer.Points, secondPlayer.Points);
        }
        
        
        #endregion
        
    }

    public class AverageParameters
    {
        public double AvgAces { get; set; }

        public double Avg2w { get; set; }

        public double AvgAw { get; set; }

        public double AvgBpw { get; set; }

        public double AvgRpw { get; set; }
    }

    public class CommonParameters
    {
        public double ServeAdvFirstPlayer { get; set; }

        public double ServeAdvSecondPlayer { get; set; }

        public double ServeAdv { get; set; }

        public double TwoW { get; set; }

        public double BPW { get; set; }

        public double Aces { get; set; }

        public double Points { get; set; }

        public double WinnerFirstPlayer { get; set; }

        public double WinnerSecondPlayer { get; set; }

        public double Win { get; set; }

        public double Calculate()
        {
            return (ServeAdv + TwoW + BPW + Aces + Points + Win) / 6;
        }
    }

}