﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Freelansim.ProbabilityCalc.Calculator.Calculator;
using Freelansim.ProbabilityCalc.Calculator.Models;

namespace Freelansim.ProbabilityCalc.Calculator.Interfaces
{
    public interface IProbabilityCalculator
    {
        ProbabilityResult Calculate(List<ProbabilitySource> data);

        Task<ProbabilityResult> CalculateAsync(List<ProbabilitySource> data);

        AverageParameters CalculateAvgParameters(List<IStatistics> statisticsByPeriod);

        CommonParameters CalculateCommonParameters(AverageParameters first, AverageParameters second,
            double firstWinStat, double secondWinStat,  int pointsFirst, int pointsSecond);
    }
}
