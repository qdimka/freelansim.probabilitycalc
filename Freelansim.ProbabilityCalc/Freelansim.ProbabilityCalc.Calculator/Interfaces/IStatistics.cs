﻿using System;

namespace Freelansim.ProbabilityCalc.Calculator.Interfaces
{
    public interface IStatistics
    {
        long Id { get; set; }
        double Aces { get; set; }
        double F1W { get; set; }
        double F2W { get; set; }
        double BPW { get; set; }
        double RPW { get; set; }
        bool IsWinner { get; set; }
        double Aw { get; }
        long TourId { get; set; }
        string Court { get; set; }
        DateTime TourDate { get; set; }
    }
}