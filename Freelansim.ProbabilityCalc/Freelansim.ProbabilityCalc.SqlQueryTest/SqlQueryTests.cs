﻿using System;
using System.Linq;
using Freelansim.ProbabilityCalc.Dal.Repository;
using Freelansim.ProbabilityCalc.Domain.Helpers;
using Freelansim.ProbabilityCalc.Domain.Model;
using NUnit.Framework;

namespace Freelansim.ProbabilityCalc.SqlQueryTest
{
    [TestFixture]
    public class SqlQueryTests
    {
        private readonly string connection 
            = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\OnCourt.mdb;Jet OLEDB:Database Password=qKbE8lWacmYQsZ2;";
        
        private IRepository _repository;

        [SetUp]
        public void StartUp()
        {
            _repository = new AccessRepository(connection);
        }

        [Test]
        public void Players_With_Fields()
        {
            var players = _repository.Get<Player>(SqlQueries.Players);
            Assert.IsNotNull(players);
        }
        
        [Test]
        public void Courts_With_Fields()
        {
            var courts = _repository.Get<Court>(SqlQueries.Courts);
            Assert.IsNotNull(courts);
        }
        
        [Test]
        public void Stats_With_Fields()
        {
            var stats = _repository.Get<Statistics>(SqlQueries.Courts);
            Assert.IsNotNull(stats);
        }
        
        [Test]
        public void Tours_With_Fields()
        {
            var tours = _repository.Get<Tour>(SqlQueries.Tours);
            Assert.IsNotNull(tours);
        }
        
        [Test]
        public void StatByYear_With_Fields()
        {
            var stats = _repository.Get<Statistics>(SqlQueries.StatsByLastYear(1, new DateTime(2008, 6, 6)));

            Assert.IsNotNull(stats);
        }
        
        [Test]
        public void StatLastTen_With_Fields()
        {
            var stats = _repository.Get<Statistics>(SqlQueries.StatsByLastTenTours(19));

            Assert.IsNotNull(stats);
            Assert.AreEqual(10, stats.Count());
        }
    }
}