﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Freelansim.ProbabilityCalc.Domain.Model;

namespace Freelansim.ProbabilityCalc.Dal.Repository
{
    public class FakeRepository : IRepository
    {
        public IEnumerable<T> Get<T>(string query) where T : class
        {
            var type = typeof(T).Name;
            switch (type)
            {
                case "Player": 
                    return  new List<Player>()
                    {
                        new Player() {Id = 1, Name = "Player_1"},
                        new Player() {Id = 2, Name = "Player_2"},
                        new Player() {Id = 3, Name = "Player_3"},
                        new Player() {Id = 4, Name = "Player_4"},
                    } as List<T>;
                
                case "Court": 
                    return  new List<Court>()
                    {
                        new Court() {Id = 1, Name = "Court_1"},
                        new Court() {Id = 2, Name = "Court_2"},
                        new Court() {Id = 3, Name = "Court_3"},
                        new Court() {Id = 4, Name = "Court_4"},
                    } as List<T>;
                
                default:
                    throw new InvalidOperationException();
 
            }
        }

        public Task<IEnumerable<T>> GetAsync<T>(string query) where T : class
        {
            throw new NotImplementedException();
        }
    }
}