﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Threading.Tasks;
using Dapper;
using Freelansim.ProbabilityCalc.Domain.Helpers;
using Freelansim.ProbabilityCalc.Domain.Model;

namespace Freelansim.ProbabilityCalc.Dal.Repository
{
    public class AccessRepository : IRepository
    {
        private readonly string _connection;

        public AccessRepository(string connection)
        {
            _connection = connection.Contains("Provider=") 
                ? connection 
                : ConfigurationManager.ConnectionStrings[connection].ConnectionString;

            SqlMapper.SetTypeMap(typeof(Player), new ColumnAttributeTypeMapper<Player>());
            SqlMapper.SetTypeMap(typeof(Court), new ColumnAttributeTypeMapper<Court>());
            SqlMapper.SetTypeMap(typeof(Statistics), new ColumnAttributeTypeMapper<Statistics>());
            SqlMapper.SetTypeMap(typeof(Tour), new ColumnAttributeTypeMapper<Tour>());
        }

        public IEnumerable<T> Get<T>(string query) where T : class
        {
            using (var connection = new OleDbConnection(_connection))
            {
                return connection.Query<T>(query);
            }
        }

        public Task<IEnumerable<T>> GetAsync<T>(string query) where T : class
        {
            using (var connection = new OleDbConnection(_connection))
            {
                return connection.QueryAsync<T>(query);
            }
        }
    }
}
