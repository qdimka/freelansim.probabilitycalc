﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Freelansim.ProbabilityCalc.Dal.Repository
{
    public interface IRepository
    {
        IEnumerable<T> Get<T>(string query) where T : class;
        
        Task<IEnumerable<T>> GetAsync<T>(string query) where T : class;
    }
}
