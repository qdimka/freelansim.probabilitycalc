﻿using System.Collections.Generic;
using System.Linq;
using Freelansim.ProbabilityCalc.Calculator.Calculator;
using Freelansim.ProbabilityCalc.Calculator.Interfaces;
using Freelansim.ProbabilityCalc.Calculator.Models;
using NUnit.Framework;

namespace Freelansim.ProbabilityCalc.CalcTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private IProbabilityCalculator calculator;

        private PlayerSource _roger;
        private PlayerSource _rafael;

        [SetUp]
        public void SetUp()
        {
            calculator = new ProbabilityCalculator();
            _roger = new PlayerSource()
            {
                Id = 19,
                Coefficient = 2,
                Name = "Roger Federer",
                Points = 8670,
                LastTenTours = new List<IStatistics>(),
                LastYearTours = new List<IStatistics>
                {
                    new Statistics() {Aces = 13, F1W = 81, F2W = 74, BPW = 56, RPW = 46},
                    new Statistics() {Aces = 5, F1W = 77, F2W = 66, BPW = 36, RPW = 0},
                    new Statistics() {Aces = 5, F1W = 74, F2W = 65, BPW = 57, RPW = 61},
                    new Statistics() {Aces = 14, F1W = 74, F2W = 56, BPW = 38, RPW = 40},
                    new Statistics() {Aces = 20, F1W = 83, F2W = 43, BPW = 42, RPW = 34},
                    new Statistics() {Aces = 11, F1W = 83, F2W = 44, BPW = 67, RPW = 39},
                    new Statistics() {Aces = 8, F1W = 75, F2W = 60, BPW = 28, RPW = 45},
                }
            };

            _rafael = new PlayerSource()
            {
                Id = 2,
                Coefficient = 3,
                Name = "Rafael Nadal",
                Points = 8770,
                LastTenTours = new List<IStatistics>(),
                LastYearTours = new List<IStatistics>
                {
                    new Statistics(){Aces = 9, F1W = 76, F2W = 53, BPW = 66, RPW = 40},
                    new Statistics(){Aces = 8, F1W = 75, F2W = 58, BPW = 43, RPW = 42},
                    new Statistics(){Aces = 6, F1W = 78, F2W = 38, BPW = 44, RPW = 46},
                    new Statistics(){Aces = 1, F1W = 68, F2W = 54, BPW = 60, RPW = 29},
                }
            };
        }

        [Test]
        public void Calculator_Return_Result()
        {
            var source = new ProbabilitySource(){FirstPlayer = _roger, SecondPlayer = _rafael};
            var result = calculator.Calculate(new List<ProbabilitySource>{source});

            Assert.IsInstanceOf(typeof(ProbabilityResult), result);
            Assert.IsFalse(result.IsEmpty);
        }

        [Test]
        public void Calculator_Return_SomeResultForPair()
        {
            var rogerRafael = new ProbabilitySource() { FirstPlayer = _roger, SecondPlayer = _rafael };
            var rafaelRoger = new ProbabilitySource() { FirstPlayer = _rafael, SecondPlayer = _roger };

            var rogerRafaelResult = calculator.Calculate(new List<ProbabilitySource> { rogerRafael }).Results.First();
            var rafaelRogerResult = calculator.Calculate(new List<ProbabilitySource> { rafaelRoger }).Results.First();

            Assert.AreEqual(rogerRafaelResult.First.Name, rafaelRogerResult.Second.Name);
            Assert.AreEqual(rogerRafaelResult.First.ProbabilityByYear, rafaelRogerResult.Second.ProbabilityByYear);
        }
    }
}
