﻿using System;
using System.Globalization;

namespace Freelansim.ProbabilityCalc.Domain.Helpers
{
    public static class SqlQueries
    {
        public static string Players => "select ID_P, NAME_P, POINT_P from players_atp where POINT_P <> 0 order by NAME_P";
        
        public static string PlayersLike(string pattern) => $"{Players} where NAME_P LIKE '{pattern}%'";

        public static string Courts => "select ID_C, NAME_C from courts";

        public static string Tours => "select ID_T, NAME_T, DATE_T from tours_atp";

        public static string Stats => "select ID1, ID2, ID_T, NAME_T, DATE_T from stat_atp";

        public static string StatsByPlayerId(int id) => $"{Stats} left join #Tours t on t.ID_T = ID_T where ID1={id} or ID2={id}";

        public static string StatsByLastTenTours(long playerId) => $@"select top 10
                                                                        IIF(st.ID1 = {playerId}, st.ID1, st.ID2) as ID,
                                                                        IIF(st.ID1 = {playerId}, st.ACES_1, st.ACES_2) as ACES,
                                                                        IIF(st.ID1 = {playerId}, (st.W1S_1*100/st.W1SOF_1),(st.W1S_2*100/st.W1SOF_2)) as 1w,
                                                                        IIF(st.ID1 = {playerId}, (st.W2S_1*100/st.W2SOF_1), (st.W2S_2*100/st.W2SOF_2)) as 2w,
                                                                        IIF(st.ID1 = {playerId}, (st.BP_1*100/st.BPOF_1), (st.BP_2*100/st.BPOF_2))      as bpw,
                                                                        IIF(st.ID1 = {playerId}, (st.RPW_1*100/st.RPWOF_1), (st.RPW_2*100/st.RPWOF_2)) as rpw,
                                                                    
                                                                        IIF(st.ID1 = {playerId}, 1, 0) as WINNER,
                                                                    
                                                                        t.ID_T as ID_T,
                                                                        t.DATE_T as DATE_T,
                                                                        t.ID_C_T as ID_C_T
                                                                    from stat_atp st 
                                                                    left join tours_atp t on t.ID_T = st.ID_T
                                                                    where (st.ID1={playerId} or st.ID2={playerId})
                                                                        and st.W1SOF_1 <> 0 and st.W1SOF_2 <> 0
                                                                        and st.W2SOF_1 <> 0 and st.W2SOF_2 <> 0
                                                                        and st.BPOF_1 <> 0 and st.BPOF_2 <> 0
                                                                        and st.RPWOF_1 <> 0 and st.RPWOF_2 <> 0
                                                                    order by t.DATE_T desc, st.ID1, st.ID2, st.ACES_1, st.ACES_2 asc";

        public static string StatsByLastYear(long playerId, DateTime startDate)
        {
            var start = startDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            var end = startDate.AddDays(-365).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            return                                                 $@"select 
                                                                        IIF(st.ID1 = {playerId}, st.ID1, st.ID2) as ID,
                                                                        IIF(st.ID1 = {playerId}, st.ACES_1, st.ACES_2) as ACES,
                                                                        IIF(st.ID1 = {playerId}, (st.W1S_1*100/st.W1SOF_1),(st.W1S_2*100/st.W1SOF_2)) as 1w,
                                                                        IIF(st.ID1 = {playerId}, (st.W2S_1*100/st.W2SOF_1), (st.W2S_2*100/st.W2SOF_2)) as 2w,
                                                                        IIF(st.ID1 = {playerId}, (st.BP_1*100/st.BPOF_1), (st.BP_2*100/st.BPOF_2))      as bpw,
                                                                        IIF(st.ID1 = {playerId}, (st.RPW_1*100/st.RPWOF_1), (st.RPW_2*100/st.RPWOF_2)) as rpw,
                                                                    
                                                                        IIF(st.ID1 = {playerId}, 1, 0) as WINNER,
                                                                    
                                                                        t.ID_T as ID_T,
                                                                        t.DATE_T as DATE_T,
                                                                        t.ID_C_T as ID_C_T
                                                                    from stat_atp st 
                                                                    left join tours_atp t on t.ID_T = st.ID_T
                                                                    where (st.ID1={playerId} or st.ID2={playerId}) 
                                                                        and st.W1SOF_1 <> 0 and st.W1SOF_2 <> 0
                                                                        and st.W2SOF_1 <> 0 and st.W2SOF_2 <> 0
                                                                        and st.BPOF_1 <> 0 and st.BPOF_2 <> 0
                                                                        and st.RPWOF_1 <> 0 and st.RPWOF_2 <> 0
                                                                    and t.DATE_T between #{start}# and #{end}#
                                                                    order by t.DATE_T desc, st.ID1, st.ID2, st.ACES_1, st.ACES_2 asc";
        }
    }
}