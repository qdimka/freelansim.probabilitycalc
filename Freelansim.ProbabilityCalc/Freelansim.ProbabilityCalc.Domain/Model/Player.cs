﻿using Freelansim.ProbabilityCalc.Domain.Helpers;

namespace Freelansim.ProbabilityCalc.Domain.Model
{
    public class Player
    {
        [ColumnName("ID_P")]
        public long Id { get; set; }

        [ColumnName("NAME_P")]
        public string Name { get; set; }

        [ColumnName("POINT_P")]
        public int Points { get; set; }
    }
}
