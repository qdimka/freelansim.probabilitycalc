﻿using System;
using Freelansim.ProbabilityCalc.Domain.Helpers;

namespace Freelansim.ProbabilityCalc.Domain.Model
{
    public class Statistics
    {
        #region Игрок

        [ColumnName("ID")]
        public long Id { get; set; }
        
        [ColumnName("ACES")]
        public double Aces { get; set; }
        
        [ColumnName("1w")]
        public double F1W { get; set; }
        
        [ColumnName("2w")]
        public double F2W { get; set; }
        
        [ColumnName("bpw")]
        public double BPW { get; set; }
        
        [ColumnName("rpw")]
        public double RPW { get; set; }

        [ColumnName("WINNER")]
        public bool IsWinner { get; set; }
       
        public double Aw => (F1W + F2W) / 2;

        #endregion

        #region Турнир

        [ColumnName("ID_T")]
        public long TourId { get; set; }

        [ColumnName("ID_C_T")]
        public string Court { get; set; }

        [ColumnName("DATE_T")]
        public DateTime TourDate { get; set; }

        #endregion
        
    }
}
