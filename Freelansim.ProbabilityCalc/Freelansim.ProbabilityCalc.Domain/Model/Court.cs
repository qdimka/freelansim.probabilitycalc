﻿using Freelansim.ProbabilityCalc.Domain.Helpers;

namespace Freelansim.ProbabilityCalc.Domain.Model
{
    public class Court
    {
        [ColumnName("ID_C")]
        public int Id { get; set; }
        
        [ColumnName("NAME_C")]
        public string Name { get; set; }
    }
}
