﻿using System;
using Freelansim.ProbabilityCalc.Domain.Helpers;

namespace Freelansim.ProbabilityCalc.Domain.Model
{
    public class Tour
    {
        [ColumnName("ID_T")]
        public long Id { get; set; }

        [ColumnName("NAME_T")]
        public string Name { get; set; }

        [ColumnName("DATE_T")]
        public DateTime Date { get; set; }
    }
}
