﻿using Freelansim.ProbabilityCalc.Client.ViewModels.SelectData;
using Freelansim.ProbabilityCalc.Domain.Model;

namespace Freelansim.ProbabilityCalc.Client.Models
{
    public class NewPairBody
    {
        public PlayerViewModel First { get; set; }
        
        public PlayerViewModel Second { get; set; }

        public Court Court { get; set; }
    }
}