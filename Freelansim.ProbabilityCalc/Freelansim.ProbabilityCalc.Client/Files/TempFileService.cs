﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelansim.ProbabilityCalc.Client.Files
{
    public class TempFileService : IFileService
    {
        public string CreateFromSource<T>(string header, List<T> source)
        {
            var fileName = TempFileName();
            var strings = source.Select(x => x.ToString()).ToList();
            strings.Insert(0, header);

            File.AppendAllLines(fileName, strings, Encoding.UTF8);

            return fileName;
        }

        public void OpenFile(string path)
        {
            Process.Start(path);
        }

        private string TempFileName() => $"{Path.GetTempPath()}\\{Guid.NewGuid():N}.csv";
    }
}
