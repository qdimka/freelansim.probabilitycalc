﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelansim.ProbabilityCalc.Client.Files
{
    public interface IFileService
    {
        string CreateFromSource<T>(string header, List<T> source);

        void OpenFile(string path);
    }
}
