﻿namespace Freelansim.ProbabilityCalc.Client.Messenger.Message
{
    public class Message<T> where T : class
    {
        public T Body { get; set; }

        public bool IsEmpty 
            => Body == null;
    }
}
