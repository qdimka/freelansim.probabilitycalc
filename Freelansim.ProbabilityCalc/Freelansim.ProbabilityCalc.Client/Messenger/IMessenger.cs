﻿using Freelansim.ProbabilityCalc.Client.Messenger.Message;

namespace Freelansim.ProbabilityCalc.Client.Messenger
{
    public interface IMessenger
    {
        void RegisterObserver(string type, ColleguageBase colleguage, string instanceName = null);

        void UnRegisterObserver(string type, string instanceName = null);

        void SendMessage<T>(string type, Message<T> message, string instanceName = null) where T : class;

        bool Exists(string type, string instanceName = null);
    }
}
