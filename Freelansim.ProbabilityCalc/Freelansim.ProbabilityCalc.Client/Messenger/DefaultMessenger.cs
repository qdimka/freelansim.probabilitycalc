﻿using System.Collections.Generic;
using System.Linq;
using Freelansim.ProbabilityCalc.Client.Messenger.Message;

namespace Freelansim.ProbabilityCalc.Client.Messenger
{
    public class DefaultMessenger : IMessenger
    {
        private IList<ColleguageItem> observers;

        public DefaultMessenger()
        {
            observers = new List<ColleguageItem>();
        }

        public bool Exists(string type, string instanceName = null) => observers.Any(o => o.InstanceName == instanceName && o.Type == type);    

        public void RegisterObserver(string type, ColleguageBase colleguage, string instanceName = null)
        {
            if (type != null)
            {
                if (Exists(type, instanceName))
                    UnRegisterObserver(type, instanceName);

                observers.Add(new ColleguageItem { Type = type.ToString(), Colleguage = colleguage, InstanceName = instanceName });
            }
        }

        public void SendMessage<T>(string type, Message<T> message, string instanceName = null) 
            where T : class
        {
            if (type != null && observers.Any())
            {
                if (!Exists(type, instanceName))
                    return;

                observers.FirstOrDefault(o => o.InstanceName == instanceName && o.Type == type)?
                         .Colleguage
                         .Notify(message);
            }
        }

        public void UnRegisterObserver(string type, string instanceName = null)
        {
            if (type != null)
            {
                if (!Exists(type, instanceName))
                    return;

                observers.Remove(observers.First(o => o.InstanceName == instanceName && o.Type == type));
            }
        }
    }

    public class ColleguageItem
    {
        public string Type { get; set; }

        public string InstanceName { get; set; }

        public ColleguageBase Colleguage { get; set; }
    }
}
