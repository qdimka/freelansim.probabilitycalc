﻿using Freelansim.ProbabilityCalc.Client.Messenger.Message;

namespace Freelansim.ProbabilityCalc.Client.Messenger
{
    public abstract class ColleguageBase
    {
        public virtual void Notify<T>(Message<T> message) where T : class { }
    }
}
