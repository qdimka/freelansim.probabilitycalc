﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace Freelansim.ProbabilityCalc.Client.Command
{
    public interface IAsyncCommand : IAsyncCommand<object>
    {

    }

    public interface IAsyncCommand<T> : ICommand
    {
        Task ExecuteAsync(T obj);
    }
}
