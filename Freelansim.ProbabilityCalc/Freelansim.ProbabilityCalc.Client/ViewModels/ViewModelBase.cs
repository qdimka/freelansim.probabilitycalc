﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using Freelansim.ProbabilityCalc.Client.Messenger;

namespace Freelansim.ProbabilityCalc.Client.ViewModels
{
    /// <summary>
    /// Абстракция
    /// </summary>
    public abstract class ViewModelBase : ColleguageBase, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(Expression<Func<object>> property)
            => OnPropertyChanged((property.Body as MemberExpression)?.Member.Name);

        protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
