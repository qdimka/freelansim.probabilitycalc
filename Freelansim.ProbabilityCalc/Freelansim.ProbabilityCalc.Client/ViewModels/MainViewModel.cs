﻿using Freelansim.ProbabilityCalc.Client.Messenger;
using Freelansim.ProbabilityCalc.Client.ViewModels.SelectData;
using Freelansim.ProbabilityCalc.Dal.Repository;

namespace Freelansim.ProbabilityCalc.Client.ViewModels
{
    /// <summary>
    /// Корневая модель
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private ViewModelBase _currentViewModel;
        private readonly IRepository _repository;
        private IMessenger _messenger;

        public MainViewModel()
        {
            _repository = new AccessRepository("Main");
            _messenger = new DefaultMessenger();
            _currentViewModel = new SelectViewModel(_repository, _messenger);

            _messenger.RegisterObserver(typeof(MainViewModel).FullName, this);
            _messenger.RegisterObserver(typeof(SelectViewModel).FullName, CurrentViewModel);
        }

        /// <summary>
        /// Текущая отображаемая ViewModel 
        /// </summary>
        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

    }
}
