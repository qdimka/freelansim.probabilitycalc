﻿using System;
using Freelansim.ProbabilityCalc.Domain.Model;

namespace Freelansim.ProbabilityCalc.Client.ViewModels.SelectData
{
    /// <summary>
    /// Модель для представления пары игроков
    /// </summary>
    public class PlayerPairViewModel : ViewModelBase
    {
        private bool _selected;

        public PlayerPairViewModel(PlayerViewModel first, PlayerViewModel second, Court court)
        {
            First = first ?? throw new InvalidOperationException(nameof(first));
            Second = second ?? throw new InvalidOperationException(nameof(second));
            Court = court ?? throw new InvalidOperationException(nameof(court));
        }

        /// <summary>
        /// Первый игрок
        /// </summary>
        public PlayerViewModel First { get; }

        /// <summary>
        /// Второй игрок
        /// </summary>
        public PlayerViewModel Second { get; }

        /// <summary>
        /// Покрытие
        /// </summary>
        public Court Court { get; set; }
        
        /// <summary>
        /// На будущее, если захотим чекбоксы в списке
        /// </summary>
        public bool Selected
        {
            get => _selected;
            set
            {
                _selected = value;
                OnPropertyChanged();
            }
        }
    }
}
