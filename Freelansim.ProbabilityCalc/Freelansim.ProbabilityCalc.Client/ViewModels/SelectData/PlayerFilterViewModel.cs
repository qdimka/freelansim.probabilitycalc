﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Freelansim.ProbabilityCalc.Dal.Repository;
using Freelansim.ProbabilityCalc.Domain.Helpers;
using Freelansim.ProbabilityCalc.Domain.Model;

namespace Freelansim.ProbabilityCalc.Client.ViewModels.SelectData
{
    public class PlayerFilterViewModel : ViewModelBase
    {
        private PlayerViewModel _player;

        private ObservableCollection<PlayerViewModel> _playersCollectionView;

        public PlayerFilterViewModel(IRepository repository)
        {
            Task.Factory.StartNew(() =>
            {
                Application.Current.Dispatcher.InvokeAsync(async () =>
                {
                    var players = await repository
                        .GetAsync<Player>(SqlQueries.Players);
                    
                    Items = new ObservableCollection<PlayerViewModel>(players.Select(x => new PlayerViewModel(x)));
                });
            });
        }
        
        /// <summary>
        /// Выбранный игрок
        /// </summary>
        public PlayerViewModel Player
        {
            get => _player;
            set
            {
                _player = value; 
                OnPropertyChanged();
            }
        }
        
        #region Filter

        public ObservableCollection<PlayerViewModel> Items
        {
            get => _playersCollectionView;
            set
            {
                _playersCollectionView = value;
                OnPropertyChanged();
            }
        }
        
        #endregion
    }
}