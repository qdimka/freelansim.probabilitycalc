﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Freelansim.ProbabilityCalc.Client.Command;
using Freelansim.ProbabilityCalc.Client.Messenger;
using Freelansim.ProbabilityCalc.Client.Messenger.Message;
using Freelansim.ProbabilityCalc.Client.Models;
using Freelansim.ProbabilityCalc.Dal.Repository;
using Freelansim.ProbabilityCalc.Domain.Helpers;
using Freelansim.ProbabilityCalc.Domain.Model;

namespace Freelansim.ProbabilityCalc.Client.ViewModels.SelectData
{
    public class SelectPlayerViewModel : ViewModelBase
    {
        private readonly IRepository _repository;
        private readonly IMessenger _messenger;
        private ICommand _selectCommand;
        
        private ObservableCollection<Court> _courts;
        private Court _court;

        private PlayerFilterViewModel _firstPlayer;
        private PlayerFilterViewModel _secondPlayer;

        public SelectPlayerViewModel(IRepository repository, IMessenger messenger)
        {
            _repository = repository;
            _messenger = messenger;

            Task.Factory.StartNew(() =>
            {
                Application.Current.Dispatcher.InvokeAsync(async () =>
                {
                    var courts = await _repository
                        .GetAsync<Court>(SqlQueries.Courts);

                    Courts = new ObservableCollection<Court>(courts);
                });
            });

            _firstPlayer = new PlayerFilterViewModel(repository);
            _secondPlayer = new PlayerFilterViewModel(repository);
        }
        
        #region Courts

        /// <summary>
        /// Список покрытий
        /// </summary>
        public ObservableCollection<Court> Courts
        {
            get => _courts;
            set
            {
                _courts = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Выбранное покрытие
        /// </summary>
        public Court Court
        {
            get => _court;
            set
            {
                _court = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Players

        /// <summary>
        /// Первый игрок
        /// </summary>
        public PlayerFilterViewModel FirstPlayer
        {
            get => _firstPlayer;
            set
            {
                _firstPlayer = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Второй игрок
        /// </summary>
        public PlayerFilterViewModel SecondPlayer
        {
            get => _secondPlayer;
            set
            {
                _secondPlayer = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        public ICommand SelectCommand
            => _selectCommand ?? (_selectCommand =
                   new DelegateCommand(Select, () => FirstPlayer.Player != null && SecondPlayer.Player != null && Court != null));

        #endregion
        
        #region Methods
  
        private void Select()
        {
            _messenger.SendMessage(typeof(PlayersListViewModel).FullName, new Message<NewPairBody>()
            {
                //Перед отправкой создаем новую копию элементов
                Body = new NewPairBody
                {
                    First = FirstPlayer.Player?.Clone() as PlayerViewModel,
                    Second = SecondPlayer.Player.Clone() as PlayerViewModel,
                    Court = Court
                }
            });
        }

        #endregion
    }
}