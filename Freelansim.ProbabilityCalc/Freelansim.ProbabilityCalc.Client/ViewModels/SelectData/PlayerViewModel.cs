﻿using System;
using Freelansim.ProbabilityCalc.Domain.Model;

namespace Freelansim.ProbabilityCalc.Client.ViewModels.SelectData
{
    /// <summary>
    /// Модель представления игрока
    /// </summary>
    public class PlayerViewModel : ViewModelBase, ICloneable
    {
        private Player _player;
        private double _coefficient;
        private bool _visible;

        public PlayerViewModel(Player player)
        {
            _player = player;
            Visible = true;
        }

        public Player Player
        {
            get => _player;
            set
            {
                _player = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _player.Name;
            set
            {
                _player.Name = value;
                OnPropertyChanged();
            }
        }

        public double Coefficient
        {
            get => _coefficient;
            set
            {
                _coefficient = value; 
                OnPropertyChanged();
            }
        }

        public bool Visible
        {
            get => _visible;
            set
            {
                _visible = value; 
                OnPropertyChanged();
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}