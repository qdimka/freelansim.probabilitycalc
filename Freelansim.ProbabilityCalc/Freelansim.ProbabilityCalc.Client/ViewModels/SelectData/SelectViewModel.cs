﻿using System;
using Freelansim.ProbabilityCalc.Client.Messenger;
using Freelansim.ProbabilityCalc.Dal.Repository;

namespace Freelansim.ProbabilityCalc.Client.ViewModels.SelectData
{
    /// <summary>
    /// ViewModel для выбора и настройки параметров
    /// </summary>
    public class SelectViewModel : ViewModelBase
    {
        private readonly IRepository _repository;
        private readonly IMessenger _messenger;

        private SelectPlayerViewModel _selectPlayerViewModel;
        private PlayersListViewModel _playerListViewModel;

        [Obsolete("DesignTime")]
        public SelectViewModel()
        {
            
        }

        public SelectViewModel(IRepository repository, IMessenger messenger)
        {
            _repository = repository;
            _messenger = messenger;

            _selectPlayerViewModel = new SelectPlayerViewModel(_repository, _messenger);
            _playerListViewModel = new PlayersListViewModel(_repository, _messenger);
            
            _messenger.RegisterObserver(typeof(PlayersListViewModel).FullName, _playerListViewModel);
        }

        /// <summary>
        /// Первый игрок
        /// </summary>
        public SelectPlayerViewModel SelectPlayerViewModel
        {
            get => _selectPlayerViewModel;
            set
            {
                _selectPlayerViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Список выбранных для расчета пар игроков
        /// </summary>
        public PlayersListViewModel PlayersList
        {
            get => _playerListViewModel;
            set
            {
                _playerListViewModel = value;
                OnPropertyChanged();
            }
        }

    }
}