﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Freelansim.ProbabilityCalc.Calculator.Calculator;
using Freelansim.ProbabilityCalc.Calculator.Interfaces;
using Freelansim.ProbabilityCalc.Calculator.Models;
using Freelansim.ProbabilityCalc.Client.Command;
using Freelansim.ProbabilityCalc.Client.Files;
using Freelansim.ProbabilityCalc.Client.Messenger;
using Freelansim.ProbabilityCalc.Client.Messenger.Message;
using Freelansim.ProbabilityCalc.Client.Models;
using Freelansim.ProbabilityCalc.Dal.Repository;
using Freelansim.ProbabilityCalc.Domain.Helpers;

namespace Freelansim.ProbabilityCalc.Client.ViewModels.SelectData
{
    /// <summary>
    /// Модель представления списка пар игроков
    /// </summary>
    public class PlayersListViewModel : ViewModelBase
    {
        private readonly IRepository _repository;
        private readonly IMessenger _messenger;
        
        private ICommand _calculateCommand;
        private ICommand _deleteCommand;
        private IProbabilityCalculator _calculator;
        private IFileService _fileService;

        [Obsolete("DesignTime")]
        public PlayersListViewModel()
        {
            Pairs = new ObservableCollection<PlayerPairViewModel>();
        }

        public PlayersListViewModel(IRepository repository, IMessenger messenger)
        {
            _repository = repository;
            _messenger = messenger;
            _calculator = new ProbabilityCalculator();
            _fileService = new TempFileService();

            Pairs = new ObservableCollection<PlayerPairViewModel>();
        }

        /// <summary>
        /// Список пар игроков
        /// </summary>
        public ObservableCollection<PlayerPairViewModel> Pairs { get; set; }

        #region Command

        public ICommand CalculateCommand =>
            _calculateCommand ?? (_calculateCommand = new DelegateCommandAsync(Calculate, Pairs.Any));

        public ICommand DeleteCommand =>
            _deleteCommand ?? (_deleteCommand = new DelegateCommand<PlayerPairViewModel>(Delete, pair => pair != null));

        #endregion

        #region Methods

        private async Task Calculate()
        {
            await Task.Run(async () =>
            {
                var sourceList = new List<ProbabilitySource>();

                foreach (var pair in Pairs)
                {
                    var firstByLastTen = await _repository
                        .GetAsync<Domain.Model.Statistics>(SqlQueries.StatsByLastTenTours(pair.First.Player.Id));
                    var firstByYear = await _repository
                        .GetAsync<Domain.Model.Statistics>(SqlQueries.StatsByLastYear(pair.First.Player.Id,
                            DateTime.Now.Date));

                    var secondByLastTen = await _repository
                        .GetAsync<Domain.Model.Statistics>(SqlQueries.StatsByLastTenTours(pair.Second.Player.Id));
                    var secondByYear = await _repository
                        .GetAsync<Domain.Model.Statistics>(SqlQueries.StatsByLastYear(pair.Second.Player.Id,
                            DateTime.Now.Date));

                    sourceList.Add(new ProbabilitySource
                    {
                        FirstPlayer = new PlayerSource()
                        {
                            Id = pair.First.Player.Id,
                            Name = pair.First.Player.Name,
                            Coefficient = pair.First.Coefficient,
                            LastTenTours = firstByLastTen
                                .Select(_mapperFunc)
                                .ToList(),
                            LastYearTours = firstByYear
                                .Select(_mapperFunc)
                                .ToList(),
                            Court = pair.Court.Name,

                            Points = pair.First.Player.Points
                        },
                        SecondPlayer = new PlayerSource()
                        {
                            Id = pair.Second.Player.Id,
                            Name = pair.Second.Player.Name,
                            Coefficient = pair.Second.Coefficient,
                            LastTenTours = secondByLastTen
                                .Select(_mapperFunc)
                                .ToList(),
                            LastYearTours = secondByYear
                                .Select(_mapperFunc)
                                .ToList(),
                            Court = pair.Court.Name,

                            Points = pair.Second.Player.Points
                        }
                    });
                }

                var result = await _calculator.CalculateAsync(sourceList);
                
                var file = _fileService.CreateFromSource(result.Header, result.Results);

                _fileService.OpenFile(file);
            });
        }

        private void Delete(PlayerPairViewModel pairViewModel)
        {
            Pairs.Remove(pairViewModel);
        }

        public override void Notify<T>(Message<T> message)
        {
            var body = message.Body as NewPairBody;
            if (body != null)
                Pairs.Add(new PlayerPairViewModel(body.First, body.Second, body.Court));
        }

        /// <summary>
        /// Функция для перевода из одной модели в другую, маппер
        /// </summary>
        private Func<Domain.Model.Statistics, IStatistics> _mapperFunc => (source) => new Statistics
        {
            BPW = source.BPW,
            Aces = source.Aces,
            Id = source.Id,
            RPW = source.RPW,
            Court = source.Court,
            F1W = source.F1W,
            F2W = source.F2W,
            IsWinner = source.IsWinner,
            TourDate = source.TourDate,
            TourId = source.TourId
        };

        #endregion
    }
}