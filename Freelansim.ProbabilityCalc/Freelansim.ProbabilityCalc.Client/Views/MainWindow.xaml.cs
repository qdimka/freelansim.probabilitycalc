﻿using System.Windows;
using Freelansim.ProbabilityCalc.Client.ViewModels;

namespace Freelansim.ProbabilityCalc.Client.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainViewModel();
        }
       
    }
}
