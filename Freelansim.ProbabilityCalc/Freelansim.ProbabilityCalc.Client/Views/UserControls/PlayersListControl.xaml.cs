﻿using System.Windows.Controls;

namespace Freelansim.ProbabilityCalc.Client.Views.UserControls
{
    /// <summary>
    /// Interaction logic for PlayersListControl.xaml
    /// </summary>
    public partial class PlayersListControl : UserControl
    {
        public PlayersListControl()
        {
            InitializeComponent();
        }
    }
}
