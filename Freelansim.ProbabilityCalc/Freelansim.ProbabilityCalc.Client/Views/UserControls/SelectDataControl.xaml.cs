﻿using System.Windows.Controls;

namespace Freelansim.ProbabilityCalc.Client.Views.UserControls
{
    /// <summary>
    /// Interaction logic for SelectDataControl.xaml
    /// </summary>
    public partial class SelectDataControl : UserControl
    {
        public SelectDataControl()
        {
            InitializeComponent();
        }
    }
}
