﻿using System.Windows.Controls;
using System.Windows.Input;

namespace Freelansim.ProbabilityCalc.Client.Views.UserControls
{
    public partial class PlayerControl : UserControl
    {
        public PlayerControl()
        {
            InitializeComponent();
        }

        private void UIElement_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(sender is ComboBox comboBox)
                comboBox.IsDropDownOpen = true;
        }
    }
}
